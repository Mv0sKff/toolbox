#!/bin/bash

if [[ $1 -eq "-h" || $1 == "--help" ]]
then
	printf "Syntax: netcfg <ACTION> <INTERFACES>\n"
	printf "\nActions:\n\tup\tSet the following interface to monitor control.\n\tdown\tSet the following interface to managed control.\n\trename\tSet the name of the following interface to the after next.\n"
	printf "\nExamples:\n\tsudo netcfg up wlan0\n\tsudo netcfg down wlan0\n\tsudo netcfg rename wlx08beac05d45e wlan0\n"
	exit 0
fi

if [ "$(whoami)" == "root" ];
then
    echo "Start ${0##*/}"
else
    echo "This script requires root permissions"
    printf "Syntax: netcfg <ACTION> <INTERFACES>\n"
    printf "\nActions:\n\tup\tSet the following interface to monitor control.\n\tdown\tSet the following interface to managed control.\n\trename\tSet the name of the following interface to the after next.\n"
    printf "\nExamples:\n\tsudo netcfg up wlan0\n\tsudo netcfg down wlan0\n\tsudo netcfg rename wlx08beac05d45e wlan0\n"
    exit 1
fi

iface=$2
niface=$3
#echo $iface

up() {
        ip link set $iface down
        iw $iface set monitor control &>/dev/null
        suc=$?
        ip link set $iface up
        if [ $suc -eq 0 ]
        then
                echo "$iface: monitor control"
        else
                echo "error by changing $iface to monitor control"
        fi
}

down() {
        ip link set $iface down
        iw $iface set type managed &>/dev/null
        ip link set $iface up

#        if [ $suc == "0" ]
#        then
                echo "$iface: managed"
#        else
#                echo "error by changing $iface to managed mode"
#        fi
}

rename() {
        echo "renamed $iface to $niface"
        #iw dev | awk '$1=="Interface"{print $2}'
        ip link set $iface down
        ip link set $iface name $niface
        ip link set $niface up
        source ~/opt/netcfg/completion-netcfg.bash
}

"$@"
