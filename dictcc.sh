#!/bin/bash

if [[ $1 =~ "…" ]]
then
	echo "Contains forbidden character: \"…\" !"
	exit
fi

echo "Searching for $1:"

response=$(curl -sl "https://www.dict.cc/?s="$1 | grep 'c1Arr\|c2Arr')

str1=$(echo "$response" | grep c1Arr | sed 's/var c1Arr = new Array("",//; s/);//; s/"//g; s/ /…/g')
str2=$(echo "$response" | grep c2Arr | sed 's/var c2Arr = new Array("",//; s/);//; s/"//g; s/ /…/g')

arr1=($(echo $str1 | tr "," "\n"))
arr2=($(echo $str2 | tr "," "\n"))

for (( i=0; i<${#arr1[@]}; i++ ))
do
	printf $(( $i + 1 ))": "
	printf "$(echo ${arr1[i]} | sed 's/…/ /g')"
	printf " <> "
	printf "$(echo ${arr2[i]} | sed 's/…/ /g')"
	printf "\n"
done
